/*
This is the main js file. It houses the requirejs config and the main initialisers of our scripts.

Using requirejs lets us set up the main js components into seperate files, each component can have its own dependancies. See the js files inside scripts/components
*/
require.config({
    baseUrl: 'assets/js',
    paths: {
        jquery: '../../bower_components/jquery/jquery',
        handlebars: '../../bower_components/handlebars/handlebars'
    },
    shim: {
        handlebars: {
            exports: 'Handlebars'
        }
    }
});

// this is the main setup for requirejs. It excepts an array of dependancies and a return function with references for each required file

require([
    'jquery',
    'components/component1',
    'components/component2',
    'components/component-handlebars'
],
function ($, C1, C2, C3) {
    'use strict';
    var c1 = new C1(),
        c2 = new C2();
    console.log(c1.method1());
    console.log(c2.method1(), c2.method2());

    new C3($('#handlebars-example')).render({
        title: 'This is a custom title',
        description: 'this is a custom description'
    });
});
