define([], function(){
	'use strict';
	function inherit(class1, class2){
		for(var key in class1.prototype){
			class2.prototype[key] = class1.prototype[key];
		}
	}
	return inherit;
});