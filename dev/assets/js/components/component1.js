define(['jquery'], function(){
	'use strict';
	function component1(){
		this.name = 'component1';
	};
	component1.prototype.method1 = function(){
		return this.name+':method1';
	};
	return component1;
});