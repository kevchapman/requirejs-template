define(
	[
		'jquery',
		'helpers/inherit',
		'components/component1'
	],
	function($, inherit, C1){
		function component2(){
			C1.apply(this, arguments);
			this.name = 'component2';
		}
		component2.prototype.method2 = function(){
			return this.name+':method2';
		};
		inherit(C1, component2);
		return component2;
	}
);