define(['jquery', 'handlebars'], function($, Handlebars){
	'use strict';
	function component(ele, opts){
		var defaults = {
			name: 'component3',
			title: 'Component3 title',
			description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus, libero, expedita consequatur ratione nulla velit iusto eaque non quas pariatur cupiditate dolorem illo perferendis rerum hic reiciendis nemo cum inventore!'
		};
		this.ele = ele;
		this.template = Handlebars.compile( this.ele.find('.template').html() );
		this.opts = $.extend({}, defaults, opts);
	}
	component.prototype.render = function(obj){
		this.ele.html( this.template(obj || this.opts) );
	};

	return component;
});