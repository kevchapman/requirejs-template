require.config({
    baseUrl: 'assets/js',
    paths: {
        jquery: '../../bower_components/jquery/jquery',
        handlebars: '../../bower_components/handlebars/handlebars'
    },
    shim: {
        handlebars: {
            exports: 'Handlebars'
        }
    }
});

var result;

describe('component1', function(){
	var component1;
	// runs before each test;
	beforeEach(function(done){
		require([
			'components/component1'
		], function( Component1){
			component1 = new Component1();
			done();
		});
	});

	it('output of method1', function(){
		result = component1.method1();

		expect(result).toBe('component1:method1');
	});
});

describe('component2', function(){
	var component2;
	// runs before each test;
	beforeEach(function(done){
		require([
			'components/component2'
		], function( Component2){
			component2 = new Component2();
			done();
		});
	});

	it('output of method1', function(){
		result = component2.method1();

		expect(result).toBe('component2:method1');
	});
	it('output of method2', function(){
		result = component2.method2();

		expect(result).toBe('component2:method2');
	});
});