// Generated on 2014-04-03 based on yeoman generator-webapp 0.4.1
'use strict';
var LIVERELOAD_PORT = 35729;
var lrSnippet = require('connect-livereload')({port: LIVERELOAD_PORT});
var mountFolder = function (connect, dir) {
	return connect.static(require('path').resolve(dir));
};

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// use this if you want to recursively match all subfolders:
// 'test/spec/**/*.js'

module.exports = function (grunt) {
	// show elapsed time at the end
	require('time-grunt')(grunt);
	// load all grunt tasks
	require('load-grunt-tasks')(grunt);

	// configurable paths
	var config = {
		app: 'dev',
		dist: 'build',
		assets: 'dev/assets',
		test: 'test'
	};

	grunt.initConfig({
		config: config,
		watch: {
			compass: {
				files: ['<%= config.app %>/sass/{,*/}*.{scss,sass}'],
				tasks: ['compass:server']
			},
			livereload: {
				options: {
					livereload: LIVERELOAD_PORT
				},
				files: [
					'<%= config.app %>/*.html',
					'<%= config.assets %>/css/*.css',
					'<%= config.assets %>/js/*.js',
					'<%= config.app %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}'
				]
			}
		},
		connect: {
			options: {
				port: 9000,
				hostname: '0.0.0.0'
			},
			livereload: {
				options: {
					middleware: function (connect) {
						return [
							lrSnippet,
							mountFolder(connect, '.tmp'),
							mountFolder(connect, config.app)
						];
					}
				}
			},
			test: {
				options: {
					port: 9010,
					middleware: function (connect) {
						return [
							mountFolder(connect, 'test'),
							mountFolder(connect, 'dev')
							//mountFolder(connect, config.app)
						];
					}
				}
			},
			dist: {
				options: {
					port: 5000,
					middleware: function (connect) {
						return [
							mountFolder(connect, config.dist)
						];
					}
				}
			}
		},
		open: {
			server: {
				path: 'http://localhost:<%= connect.options.port %>'
			}
		},
		clean: {
			dist: {
				files: [{
					dot: true,
					src: [
						'.tmp',
						'<%= config.dist %>/*',
						'!<%= config.dist %>/.git*'
					]
				}]
			},
			server: '.tmp'
		},
		mocha: {
			all: {
				options: {
					run: true,
					urls: ['http://localhost:<%= connect.options.port %>/index.html']
				}
			}
		},
		compass: {
			options: {
				sassDir: '<%= config.app %>/sass',
				cssDir: '<%= config.assets %>/css',
				generatedImagesDir: '.tmp/images/generated',
				imagesDir: '<%= config.assets %>/images',
				javascriptsDir: '<%= config.assets %>/js',
				fontsDir: '<%= config.assets %>/fonts',
				importPath: '<%= config.app %>/bower_components',
				httpImagesPath: '<%= config.assets %>/images',
				httpGeneratedImagesPath: '<%= config.assets %>/images/generated',
				httpFontsPath: '<%= config.assets %>/fonts',
				relativeAssets: false
			},
			dist: {
				options: {
					generatedImagesDir: '<%= config.dist %>/images/generated'
				}
			},
			server: {
				options: {
					debugInfo: true
				}
			}
		},
		requirejs: {
			dist: {
				options: {
					almond: true,
					mainConfigFile: '<%= config.assets %>/js/main.js',
					baseUrl: '<%= config.assets %>/js',
					name: 'main',
					out: '<%= config.dist %>/assets/js/main.min.js',
					paths: {
						requireLib: '../../bower_components/requirejs/require'
					},
					include: ['requireLib']
				}
			}
		},
		rev: {
			dist: {
				files: {
					src: [
						'<%= config.dist %>/assets/js/{,*/}*.js',
						'<%= config.dist %>/assets/css/{,*/}*.css',
						'<%= config.dist %>/assets/images/{,*/}*.{png,jpg,jpeg,gif,webp}',
						'<%= config.dist %>/assets/fonts/{,*/}*.*'
					]
				}
			}
		},
		useminPrepare: {
			options: {
				dest: '<%= config.dist %>'
			},
			html: '<%= config.app %>/index.html'
		},
		usemin: {
			options: {
				dirs: ['<%= config.dist %>']
			},
			html: ['<%= config.dist %>/{,*/}*.html'],
			css: ['<%= config.dist %>/assets/css/{,*/}*.css']
		},
		imagemin: {
			dist: {
				files: [{
					expand: true,
					cwd: '<%= config.assets %>/images',
					src: '{,*/}*.{png,jpg,jpeg}',
					dest: '<%= config.dist %>/assets/images'
				}]
			}
		},
		svgmin: {
			dist: {
				files: [{
					expand: true,
					cwd: '<%= config.app %>/images',
					src: '{,*/}*.svg',
					dest: '<%= config.dist %>/images'
				}]
			}
		},
		htmlmin: {
			dist: {
				options: {
					/*removeCommentsFromCDATA: true,
					// https://github.com/yeoman/grunt-usemin/issues/44
					//collapseWhitespace: true,
					collapseBooleanAttributes: true,
					removeAttributeQuotes: true,
					removeRedundantAttributes: true,
					useShortDoctype: true,
					removeEmptyAttributes: true,
					removeOptionalTags: true*/
				},
				files: [{
					expand: true,
					cwd: '<%= config.app %>',
					src: '*.html',
					dest: '<%= config.dist %>'
				}]
			}
		},
		// Put files not handled in other tasks here
		copy: {
			dist: {
				files: [{
					expand: true,
					dot: true,
					cwd: '<%= config.app %>',
					dest: '<%= config.dist %>',
					src: [
						'*.{ico,png,txt}',
						'.htaccess',
						'assets/images/{,*/}*.{webp,gif}',
						'assets/fonts/{,*/}*.*',
						'assets/css/*.css'
					]
				}]
			},
			styles: {
				expand: true,
				dot: true,
				cwd: '<%= config.app %>/styles',
				dest: '.tmp/styles/',
				src: '{,*/}*.css'
			},
			test: {
				files: [{
					expand: true,
					dot: true,
					cwd: '<%= config.assets %>/js',
					dest: '<%= config.test %>/src',
					src: [
						'{,*/}*.js'
					]
				}]
			},
		},
		concurrent: {
			server: [
			],
			test: [
			],
			dist: [
				'compass',
				'imagemin',
				'svgmin',
				'htmlmin'
			]
		},
		bower: {
			options: {
				exclude: ['modernizr']
			},
			all: {
				rjsConfig: '<%= config.assets %>/js/main.js'
			}
		}
	});

	grunt.registerTask('server', function (target) {
		if (target === 'dist') {
			return grunt.task.run(['build', 'connect:dist:keepalive']);
		}

		grunt.task.run([
			'clean:server',
			'concurrent:server',
			'connect:livereload',
			//'open',
			'compass:server',
			'watch'
		]);
	});

	grunt.registerTask('test', [
		//'clean:server',
		//'copy:test',
		'concurrent:test',
		// 'autoprefixer',
		'connect:test:keepalive'
		//'mocha'
	]);

	grunt.registerTask('build', [
		'clean:dist',
		'useminPrepare',
		'concurrent:dist',
		//'autoprefixer',
		'concat',
		//'cssmin',
		//'uglify',
		'copy:dist',
		//'rev',
		'requirejs',
		'usemin'
	]);

	grunt.registerTask('default', [
		//'jshint',
		'test',
		'build'
	]);
};
