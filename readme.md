##requirejs demo
This is a demo of using requirejs to write and test modular javascript.

The setup of this was created using a yeoman webapp generator as a start point.

to install `npm install`

Grunt tasks

`grunt server` this runs the demo with livereload and sass compiling localhost:9000

`grunt build` this uses requirejs's build script to concatenate / minify the js into a single file.

`grunt test` runs a server with jasmine tests. localhost:9010

###requirejs

The main js file included in the html is main.js this calls components found in dev/assets/js/components. The purpose of this demo is to show how we can separate the js into individual files that each can have there own dependancies. Please see dev/assets/js/components.

###build

This task contains some features from the original yeoman generator like removing comments from HTML and compressing images. The bit we are interested in is the js concatenation.

Files are copied from dev to build. The js file results in build/assets/js/main.min.js

###Test

This task creates a server localhost:9010 to run the jasmine tests. The src for the js components points to the dev folder. This removes the need for copying the code. The test code here is very basic as it's only for demo purposes.

